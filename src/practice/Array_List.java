package practice;
import java.util.*;  
class Array_List{  
public static void main(String args[]){  
TreeSet<String> list=new TreeSet();  
list.add("Ravi");  
list.add("Vijay");  
list.add("Ravi");  
list.add("Ajay");
list.add("Ravi");
//list.add(129);
//list.add(null);
list.add("Sai");
System.out.println("Using for-each loop");
for(Object obj:list)  
    System.out.println(obj);
System.out.println("Using Iterator");
Iterator itr=list.iterator();  
while(itr.hasNext()){  
System.out.println(itr.next());  
}  
}  
}  
