package practice;
import java.util.Scanner;
public class Shapes
{
    int r,l,b,base,height;
    double pi = 3.14,perimeter,area;
    Scanner s = new Scanner(System.in);
    void perimeter()
    {
        System.out.print("Enter radius of circle:");
        r = s.nextInt();
        perimeter = 2 * pi * r;
        System.out.println("Perimeter of circle:"+perimeter);
    }
    void cirArea()
    {
    	System.out.print("Enter radius of circle:");
        r = s.nextInt();
        area =  pi * r * r;
        System.out.println("Area of circle:"+area);
    }
    void recArea()
    {
    	System.out.println("Enter length and breadth of rectangle:");
    	l = s.nextInt();
    	b = s.nextInt();
    	area = l * b;
    	System.out.println("Area of Rectangle:"+area);
       }
    void triArea()
    {
    	System.out.println("Enter base and height of triangle:");
    	base = s.nextInt();
    	height = s.nextInt();
    	area = 0.5 * base * height;
    	System.out.println("Area of Triangle:"+area);
     }
    public static void main(String[] args) 
    {
        Shapes obj = new Shapes();
        obj.perimeter();
        obj.cirArea();
        obj.recArea();
        obj.triArea();
        
    }
}