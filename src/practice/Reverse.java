package practice;
import java.util.Scanner;
public class Reverse {

	public static void main(String[] args) {
		int r,n,temp,sum=0;
		Scanner s = new Scanner(System.in);
		System.out.println("Enter a number");
		n = s.nextInt();
		 temp=n;    
		  while(n>0){    
		   r=n%10;  //getting remainder  
		   sum=(sum*10)+r;    
		   n=n/10; 
		  }
		System.out.println("Reverse of a number is:"+sum);
		

	}

}
