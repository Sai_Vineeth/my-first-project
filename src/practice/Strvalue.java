package practice;
public class Strvalue {  
	    public static void main(String[] args) {  
	        boolean b1=true;  
	        byte b2=10;    
	        short sh = 15;  
	        int i = 12; 
	        float f = 15.5f;  
	        char chr[]={'c','o','r','e','j','a','v','a'};  
	        Strvalue obj=new Strvalue();  
	        String s1 = String.valueOf(b1);    
	        String s2 = String.valueOf(b2);    
	        String s3 = String.valueOf(sh);    
	        String s4 = String.valueOf(i);    
	        String s6 = String.valueOf(f);     
	        String s8 = String.valueOf(chr);       
	        System.out.println(s1);  
	        System.out.println(s2);  
	        System.out.println(s3);  
	        System.out.println(s4);   
	        System.out.println(s6);    
	        System.out.println(s8);   
	        }
}
