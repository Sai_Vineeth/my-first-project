package practice;
     public class Strrevbuf{
	 public static void main(String args[])
	 {
	  String str1 = "AwqfhiBEFBEWGUY9BE"; 
	  StringBuffer sb1 = new StringBuffer(str1);
	  
	  System.out.println("String before reversing: " + sb1);
	  sb1.reverse();
	  System.out.println("String after reversing: " + sb1);
	 
	 }
	}


