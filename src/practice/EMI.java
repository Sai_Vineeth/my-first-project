package practice;
import java.io.*;
import java.util.Scanner; 

public class EMI { 
      
    // Function to calculate EMI 
    static float emi_calculator(float p,float r, float t) 
    { 
        float emi; 
      
        r = r / (12 * 100); // one month interest 
        t = t * 12; // one month period 
        emi = (p * r * (float)Math.pow(1 + r, t))  
                / (float)(Math.pow(1 + r, t) - 1); 
      
        return (emi); 
    } 
      
    // Driver Program 
    static public void main (String[] args) 
    { 
          
        float principal, rate, time, emi; 
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the Principal : ");
         principal = scan.nextFloat();
        System.out.print("Enter the Rate of interest : ");
         rate = scan.nextFloat();
        System.out.print("Enter the Time period : ");
         time = scan.nextFloat();
        scan.close();
          
        emi = emi_calculator(principal, rate, time); 
          
        System.out.println("Monthly EMI is = " + emi); 
    }
} 
