package first;

public class ApplicationException extends Exception {

	String s;
	Exception e;

	ApplicationException(String s) {
		this.s = s;
	}

	ApplicationException(String s, Exception e) {
		this.s =s;
		this.e = e;
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	}

	public Exception getE() {
		return e;
	}

	public void setE(Exception e) {
		this.e = e;
	}
	
	

}
