package first;

import java.io.FileNotFoundException;
import java.io.RandomAccessFile;

public class ExceptionsEx {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s = null;
		System.out.println(s.length());
		
		new ExceptionsEx().method1();
		
		/*try {
			new ExceptionsEx().method2(123123, 123123, 10000);
		} catch (ApplicationException e) {
			System.out.println(e.getS());
		}*/

	}

	void method1()  {
		try {
			RandomAccessFile rcf = new RandomAccessFile("C://1.txt", "rw");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}

		// System.exit(0);

		String s = null;
		if (s != null) {
			System.out.println(s.length());
		}

	}

	void method2(int acctA, int acctB, int i) throws ApplicationException {

		// A 1000$ B
		if (1000 < i) {
			throw new ApplicationException("Insufficent Balance");
		}
		
		if(false) {
			throw new ApplicationException("B is in active");
		}

	}
}
