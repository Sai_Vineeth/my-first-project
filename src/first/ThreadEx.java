package first;

public class ThreadEx extends Thread {
	
	public void run() {
		System.out.println("RUn method called");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("RUn method called-end");
	}
	
	public static void main(String[] args) {
		Thread t1 = new ThreadEx();
		Thread t2 = new ThreadEx();
		t1.start();
		
		t2.start();
		
	}

}
