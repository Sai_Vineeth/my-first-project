package first;
import java.io.BufferedReader;

import java.io.IOException;

import java.io.InputStreamReader;
class Helper{

String Time;

String instrutorName;

public String getTime() {

return Time;

}

public void setTime(String time) {

Time = time;

}

public String getInstrutorName() {

return instrutorName;

}

public void setInstrutorName(String instrutorName) {

this.instrutorName = instrutorName;

}

Helper(String Time,String instrutorName){

this.Time= Time;

this.instrutorName=instrutorName;

}

}



class TimesAndInstructorsHelper{

String courseName;

Helper helper;

TimesAndInstructorsHelper(String courseName,Helper helper) {

this.courseName=courseName;

this.helper=helper;

}

public String toString() { 

return courseName +" by "+ helper.getInstrutorName()+" is scheduled at "+ helper.getTime();

}

}



public class TimesAndInstructors {

public static void main(String[] args) throws IOException {



TimesAndInstructorsHelper obj1= new TimesAndInstructorsHelper("CIS101",new Helper("Mon 9 am", "Farrell"));

TimesAndInstructorsHelper obj2= new TimesAndInstructorsHelper("CIS210",new Helper("Mon 11 am", "Patel"));
TimesAndInstructorsHelper obj3= new TimesAndInstructorsHelper("MKT100",new Helper("Tues 8:30 am", "Wong"));

TimesAndInstructorsHelper obj4= new TimesAndInstructorsHelper("ACC150",new Helper("Thurs 6 pm", "Diitrich"));

TimesAndInstructorsHelper obj5= new TimesAndInstructorsHelper("CIS101",new Helper("Fri 1 pm", "Lennon"));

System.out.println("Please enter a courseName...");

BufferedReader br = new BufferedReader(new InputStreamReader(System.in));// Taking a userInput of courseName to check its schedule

String courseName=br.readLine();

if(courseName.equalsIgnoreCase(obj1.courseName))
{System.out.println(obj1.toString());}

if (courseName.equalsIgnoreCase(obj2.courseName))

System.out.println(obj2.toString());

if (courseName.equalsIgnoreCase(obj3.courseName))

System.out.println(obj3.toString());

if (courseName.equalsIgnoreCase(obj4.courseName)) 

System.out.println(obj4.toString());

if (courseName.equalsIgnoreCase(obj5.courseName)) 

System.out.println(obj5.toString());

if(!courseName.equalsIgnoreCase(obj1.courseName)&&!courseName.equalsIgnoreCase(obj2.courseName) && !courseName.equalsIgnoreCase(obj3.courseName)

&& !courseName.equalsIgnoreCase(obj4.courseName)&&!courseName.equalsIgnoreCase(obj5.courseName) ) {//if inputCourseName isn't present

System.out.println("ERROR:NOT Found");

}

}

}
