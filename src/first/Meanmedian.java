package first;
import java.util.Arrays;
import java.util.Scanner;
public class Meanmedian {
	 public static double findMean(int arr[], int n) 
	    { 
	        int sum = 0; 
	        for (int i = 0; i < n; i++)  
	            sum += arr[i];
	        if(arr[i]==9999)
	        {
	        	sum=9999-sum;
	        }
	      
	        return (double)sum / (double)n; 
	    } 
	  
	     
	    public static double findMedian(int arr[], int n) 
	    { 
	        // First we sort the array 
	        Arrays.sort(arr); 
	  
	        // check for even case 
	        if (n % 2 != 0) 
	        return (double)arr[n / 2]; 
	      
	        return (double)(arr[(n - 1) / 2] + arr[n / 2]) / 2.0; 
	    } 
	public static void main(String[] args) {
		int  arr[]=new int[20];
		Scanner s= new Scanner(System.in);
		int size=20;
		System.out.println("Enter the values into the array: ");
		for(int i=0;i<20;i++) {
			arr[i]=s.nextInt();
			if(arr[i]==9999)
			{
				break;
			}
		}
		 System.out.println(Arrays.toString(arr));
		 System.out.println("Mean = " + findMean(arr, 20));  
	     System.out.println("Median = " + findMedian(arr, 20));
	}

}
