package first;
import java.util.*;
class Recording {
	String title;
	String artist;
	int playingTime;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public int getPlayingTime() {
		return playingTime;
	}
	public void setPlayingTime(int i) {
		this.playingTime = i;
	}
	public Recording(String title, String artist, int playingTime) {
		this.title = title;
		this.artist = artist;
		this.playingTime = playingTime;
	}
}
public class RecordingSort {	
	public static void main(String[] args) {
		Recording[] list = new Recording[5];
		Scanner input = new Scanner(System.in);
		int i = 0;
		for (i = 0; i < list.length; i++) {
			int j = i + 1;			
			System.out.print("Enter song " + j + "'s title: ");
			String title = input.nextLine();			
			System.out.print("Enter song " + j + "'s artist: ");
			String artist = input.nextLine();			
			System.out.print("Enter song " + j + "'s playing time in seconds: ");
			String playingTimeString = input.nextLine();
			int playingTime = Integer.parseInt(playingTimeString);			
			list[i] = new Recording(title, artist, playingTime);		
			System.out.println();
		}			
		String sortMethod;		
		do {
			System.out.println("How should these songs be sorted? 1)'S' 2)'A' 3)'T' 4)'Q'");
			sortMethod = input.next();
			char sortMethod1=sortMethod.charAt(0);
			if (sortMethod1=='A' && sortMethod1 == 'S'&& sortMethod1=='T'&&sortMethod1=='Q') {
				int a, b;
				int highestVal = list.length - 1;			
				for (a = 0; a < highestVal; a++) {
					for (b = 0; b < highestVal; b++) {
						int c = b + 1;												
						if (sortMethod1 == 'S') {						
							if (list[b].getTitle().compareTo(list[c].getTitle()) > 0) {							
								Recording temp = list[b];
								list[b] = list[c];
								list[c] = temp;
							}
						} 	
						else if (sortMethod1 == 'A') {							
							if (list[b].getArtist().compareTo(list[c].getArtist()) > 0) {							
								Recording temp = list[b];
								list[b] = list[c];
								list[c] = temp;
							}
						} 
						else if (sortMethod1 == 'T') {						
							if (list[b].getPlayingTime() > list[c].getPlayingTime()) {								
								Recording temp = list[b];
								list[b] = list[c];
								list[c] = temp;
							}
						}
						else if(sortMethod1== 'Q') {
							break;
						}
					}
				}
			}
			else {
				System.out.println("Invalid choice. Please choose 'S', 'A', or 'T'.");
			}
		 
		while (sortMethod1 == 'A' || sortMethod1 =='S'|| sortMethod1=='T')	;
		System.out.println();		
		for (i = 0; i < list.length; i++) {
			System.out.println("Song: Title: " + list[i].getTitle() + ". Artist: " + list[i].getArtist() + ". Playing time: "
					+ list[i].getPlayingTime() + " seconds.");
		}	
		input.close();
		}
	}
}
