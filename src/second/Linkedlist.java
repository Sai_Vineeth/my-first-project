package second;
import java.util.*;  
public class Linkedlist{  
public static void main(String args[]){  
LinkedList<String> al=new LinkedList<String>();  
al.add("Ravi");  
al.add("Vijay");  
al.add("Ravi");  
al.add("Ajay");
System.out.println("\nFor Loop");
for(int i=0;i<al.size();i++) {
System.out.println(al.get(i));	
}
System.out.println("\nFor Each Loop");
for(String strtemp:al) {
	System.out.println(strtemp);
}
System.out.println("\nIterator");
Iterator<String> itr=al.iterator();  
while(itr.hasNext()){  
System.out.println(itr.next());  
}  
}  
}  
