package second;
class A
{
	public void add()
	{
		System.out.println("Addition of parent class");
	}
}
class B extends A
{
	public void add()
	{
		System.out.println("Addition of Child class");
		super.add();
	}
	
}
class TestSuper1
{
	public static void main(String[] args) {
		B ob = new B();
		ob.add();
		
	}
}