package second;
public class Compstr {
	public static void main(String[] args) {
	String str1="Sai";
	String str2="Sai";
	String str3=new String("Sai");
	String str4="Vineeth";
	String str5= new String("Vineeth");
	String str6=new String("Vineeth");
	System.out.println("\nUsing equals method");
	System.out.println(str1.equals(str2));
	System.out.println(str2.equals(str3));
	System.out.println(str3.equals(str4));
	System.out.println("\nusing == operator");
	System.out.println(str1==str2);
	System.out.println(str2==str3);
	System.out.println(str3==str4);
	System.out.println(str4==str5);
	System.out.println("\nusing compareTo");
	System.out.println(str1.compareTo(str2));  
	System.out.println(str1.compareTo(str3));  
	System.out.println(str3.compareTo(str1)); 
	}
}
