package second;
import java.util.*;  
public class VectorTest{  
public static void main(String args[]){  
Vector<String> v=new Vector<String>();  
v.add("Ayush");  
v.add("Amit");  
v.add("Ashish");  
v.add("Garima");
System.out.println("\nFor Loop");
for(int i=0;i<v.size();i++) {
	System.out.println(v.get(i));
}
System.out.println("\nFor Each Loop");
for(String strtemp:v)
{
	System.out.println(strtemp);
}
System.out.println("\nIterator");
Iterator<String> itr=v.iterator();  
while(itr.hasNext()){  
System.out.println(itr.next());  
}  
}  
}  

