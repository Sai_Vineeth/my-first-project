package second;
public class Excep2{  
	   static void validate(int salary){  
	     if(salary<1000)  
	      throw new ArithmeticException("no bonus");  
	     else  
	      System.out.println("Bonus granted");  
	   }  
	   public static void main(String args[]){  
	      validate(7990);  
	      System.out.println("rest of the code...");  
	  }  
	}  