package second;
class Testarray{  
public static void main(String args[]){  
int a[]=new int[5];  
a[0]=20; 
a[1]=300;  
a[2]=10;  
a[3]=60;  
a[4]=90; 
String str[]=new String[5];
str[0]="Sam";
str[1]="Bob";
str[2]="Tom";
str[3]="Sai";
str[4]="Alex";
System.out.println("Using for loop");
for(int i=0;i<a.length;i++)//normal for loop  
System.out.println(a[i]);
for(int j=0;j<str.length;j++)
	System.out.println(str[j]);
System.out.println("\nUsing for-each loop");
for(int i:a)  //for each loop
System.out.println(i);
for(String strtemp:str)
	System.out.println(strtemp);
}
}  