package second;
import java.io.IOException;  
class Excep3{  
  void m()throws IOException{  
    throw new IOException("device error");  
  }  
  void n()throws IOException{  
    m();  
  }  
  void p(){  
   try{  
    n();  
   }catch(Exception e){System.out.println("exception handled");}  
  }  
  public static void main(String args[]){  
   Excep3 obj=new Excep3();  
   obj.p();  
   System.out.println("normal flow...");  
  }  
}  
