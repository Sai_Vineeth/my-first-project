package second;
import java.util.*;  
class Arraylist{  
public static void main(String args[]){  
ArrayList<String> list=new ArrayList<String>();//Creating arraylist  
list.add("Ravi");//Adding object in arraylist  
list.add("Vijay");  
list.add("Ravi");  
list.add("Ajay");
System.out.println("\nUsing For LOOP");
for(int i=0;i<list.size();i++) {
	System.out.println(list.get(i));
}
System.out.println("\nUsing For-each LOOP");
for(String strtemp: list) {
	System.out.println(strtemp);
}
System.out.println("\nTraversing list through Iterator");  
Iterator itr=list.iterator();  
while(itr.hasNext()){  
System.out.println(itr.next());  
}  
}  
}  
