package second;
abstract class Bike{    
	   abstract void run();    
	 }  
abstract class Car{
	abstract void run();
}
	//Creating a Child class which inherits Abstract class  
	 class Honda extends Bike,Car{  
	 void run(){
		 System.out.println("running safely..");
		 }  
	 }
	 //Creating a Test class which calls abstract and non-abstract methods  
	 class TestAbstraction{  
	 public static void main(String args[]){  
	  Honda obj = new Honda ();  
	  obj.run(); 
	 }  
	}  